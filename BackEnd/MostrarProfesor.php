<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" id="ArchivoCss" type="text/css" href="../FrontEnd/Assets/stylo.css">
    <title>Pagina de Busqueda de Profesores</title>
</head>
    <?php
    include '../Persistencia/conexion.php';
    $sql = 'SELECT * FROM PROFESORES';
    $result = mysqli_query($connection_mysql,$sql);
    echo $sql;
    if (!empty($result) AND mysqli_num_rows($result) > 0) { ?>
<body>
    <div class="Todos">
        <div class="All">
            <div class="head">
                <h1>Resultado de Busqueda de profesores</h1> <img id="Icono" src="../FrontEnd/Assets/icono.jpg" alt="Icono Migo">
            </div>
            <table class="egt">
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Profesion</th>
                            <th>Edad</th>
                            <th>Usuario</th>
                            <th>Contraseña</th>
                        </tr>
                        <?php while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) { ?>
                            <tr>
                                <td><?php echo $row["NombreProfesor"] ?></td>
                                <td><?php echo $row["ApellidoProfesor"] ?></td>
                                <td><?php echo $row["ProfesionProfesor"] ?></td>
                                <td><?php echo $row["EdadProfesor"] ?></td>
                                <td><?php echo $row["UsuarioProfesor"] ?></td>
                                <td><?php echo $row["ContrasenaProfesor"] ?></td>
                            </tr>
                        <?php }
                    }
                    ?>
            </table>
        </div>
    </div>
</body>
</html>