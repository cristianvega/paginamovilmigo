<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" id="ArchivoCss" type="text/css" href="">
        <title>Nuevo contenido Movilmigo</title>
    </head>

    <body>
        <div class="Todos">
            <div class="All">
                <div class="head">
                    <h1>Registro Exitoso</h1> <img id="Icono" src="../FrontEnd/Assets/icono.jpg" alt="Icono Migo">
    			</div>
    			<?php
    			if (isset($_POST["textoInicial"]) && $_POST["textoInicial"] != '' && isset($_POST["nomTema"]) && $_POST["nomTema"] != ''){
    			echo "<h1>Resumen de tareas...</h1></br>";
    				$nombreTema = str_replace(" ", "_", $_POST["nomTema"]);
    				$Tema = $_POST["tema"];
    				if(isset($_POST["SocialMedia"]) && $_POST["SocialMedia"] != ''){
    					$APPredes = $_POST["SocialMedia"];
    					$App = $APPredes;
    				}
    				if(isset($_POST["Movility"]) && $_POST["Movility"] != ''){
    					$APPtransporte = $_POST["Movility"];
    					$App = $APPtransporte;
    				}
    				if(isset($_POST["GoogleTools"]) && $_POST["GoogleTools"] != ''){
    					$APPgoogle = $_POST["GoogleTools"];
    					$App = $APPgoogle;
    				}
    				if(isset($_POST["BasicHandle"]) && $_POST["BasicHandle"] != ''){
    					$APPcelular = $_POST["BasicHandle"];
    					$App = $APPcelular;
    				}
    
    				$textoContenido = $_POST["textoInicial"];
    				echo "Se creo un archivo en el servidor de nombre: ".$nombreTema.".js";
    					if (isset($_FILES["fileContent"])){
    						$filessuccess = '';
    						$countfiles = count($_FILES['fileContent']['name']);
    						if(isset($_FILES["fileContent"])) 
    						{ 
    							$target_dir = 'ContenidosGuardados/'.$Tema.'/'.$App.'/'.$nombreTema.'/';			
    							if (!file_exists($target_dir)) {
    								mkdir($target_dir, 0777, true);
    								echo "<p>En la carpeta creada: ".$target_dir."</p>";
    							}
    							if ($countfiles>0){
    								for($i=0;$i<$countfiles;$i++){
    									$filename = basename($_FILES['fileContent']['name'][$i]);
    									$target_file = $target_dir . $filename;
    									$uploadOk = 1;
    									$fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    					     
    									// Archivos que aceptamos, por favor, no insista.
    									if (!($fileType=="pdf" || $fileType=="jpg" || $fileType=="png"|| $fileType=="doc" || $fileType=="docx" 
    									|| $fileType=="xls" || $fileType=="xlsx" || $fileType=="bmp" || $fileType=="txt" || $fileType=="eml" || $fileType=="csv")) {
    										$uploadOk = 0;
    									}
    
    						
    									if ($uploadOk == 0) {
    										echo "<br/>La extension de este archivo no es soportada: ".$filename;
    									} 
    									else {							
    										if (move_uploaded_file($_FILES['fileContent']['tmp_name'][$i],$target_file)) {
    											$filessuccess .= ' , '.$_FILES['fileContent']['tmp_name'][$i];// Sube el archivo
    										} 
    										else {
    											echo "<br/>Algo fallo con la subida de este archivo por parte del servidor: ".$filename;
    										}
    									}
    								}
    								$file = 'ContenidosGuardados/'.$Tema.'/'.$App.'/'.$nombreTema.'/'.$nombreTema.'.js';	
    								$contents =
    "import React, { Component } from 'react';
    import {Image} from 'react-native';
    import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
    	export default class AnatomyExample extends Component {
    		navigatetocontentXX=()=>{
    		this.props.navigation.navigate('".$Tema."_".$App."_".$nombreTema."')
    		} 
    		render() {
    		return (
    			<Container>
    			<Header>
    				<Body>
    				<Title>".$nombreTema."</Title>
    				</Body>
    			</Header>
    			<Content>
    				<Text>
    				".$textoContenido."
    				</Text>
    				<Text>
    					".$target_file."
    				</Text>
    				<Image source={require('../Assets/".$filename."')} style={{height: 400, width: 300 , flex: 1}}/>
    			</Content>
    			<Footer>
    				<FooterTab>
    				<Button full onPress={this.navigatetonextcontentXX}>
    					<Text>Siguiente</Text>
    				</Button>
    				</FooterTab>
    			</Footer>
    			</Container>
    			);
    		}
    	}
    	// Esta parte va a Routes.js
    	import ".$Tema."_".$App."_".$nombreTema." from '../project_modules/".$Tema."/AcademicContent/".$App."/Themes/".$nombreTema."';
    	// Esta parte va a Routes.js Navigator
    	".$Tema."_".$App."_".$nombreTema.": {
    		screen: ".$Tema."_".$App."_".$nombreTema.",
    		navigationOptions: {
    			title: '".$Tema."'
    		}
    		},
    	//Esta parte va al codigo de la pantalla del contenido para hacer el mapeo de su posicion y poder navegar
    	navigatetocontentXX=()=>{
    		this.props.navigation.navigate('".$Tema."xx')
    		} 
    	//En el boton o touchable Opacity
    	onPress={this.navigatetocontentXX}
    ";
    								file_put_contents($file, $contents);//Crea el archivo
    								//system('attrib +H ' . escapeshellarg($file));// Esconde el archivo
    							}
    						}
    					}
    				echo '</br>Muchas gracias por la ayuda!! </br> Para continuar con nuevos temas por favor clickea aqui </br> <a href="http://plataformitamovilmigo.000webhostapp.com/" class="button">Atras Para mas contenidos nuevos a subir</a>';
    			}
    			else
    			echo "<p>Aun falta informacion...</p>";
    ?>
        <?php 
            include '../Persistencia/conexion.php';
            $profesor = $_POST["profesor"];
                $sql = 'INSERT INTO LOGS_CONTENIDO (NombreContenido, TemaContenido, AplicacionContenido, FechaContenido, IDProfesor) VALUES ("'.$nombreTema.'","'.$Tema.'","'.$App.'",CURRENT_TIMESTAMP,"'.$profesor.'")';
                $result = mysqli_query($connection_mysql,$sql);
                echo $sql;
        ?>
            </div>
        </div>
    </body>
</html>