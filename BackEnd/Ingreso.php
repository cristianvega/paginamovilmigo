<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" id="ArchivoCss" type="text/css" href="../FrontEnd/Assets/stylo.css">
    <title>Pagina de automatizacion RN Movilmigo</title>
</head>
<?php 
if (isset($_POST["usuario"]) && $_POST["usuario"] != '' && isset($_POST["contrasena"]) && $_POST["contrasena"] != '')
{
    if($_POST["usuario"] == "root" && $_POST["contrasena"] == "root"){
        ?>
    <body>
    <div class="Todos">
        <div class="All">
            <div class="head">
                <h1>Nueva pantalla de contenidos</h1> <img id="Icono" src="../FrontEnd/Assets/icono.jpg" alt="Icono Migo">
            </div>
                <?php
                    echo "<h1>Bienvenido Administrador</h1>";
                    echo '<input type="hidden" id="profesor" name ="profesor" value="1"> ';
                    echo "<h1>Acciones</h1>";
                ?>
                <form enctype="multipart/form-data" name="testform" method='POST' action='BuscarProfesor.php'>
                    <input type="number" id="ProfesorID" name ="ProfesorID" placeholder="Ingresa el Numero de ID">
                    <input type="text" id="ProfesorNombre" name ="ProfesorNombre" placeholder="Ingresa el Nombre">
                    <input class="botonsubir" name="submit" type="submit" value="Buscar y Editar Profesor"/>
                </form>
                <form enctype="multipart/form-data" name="testform" method='POST' action='MostrarProfesor.php'>
                <input class="botonsubir" name="submit" type="submit" value="Mostrar Profesores"/>
                </form>
                <form enctype="multipart/form-data" name="testform" method='POST' action='MostrarLogContenido.php'>
                <input class="botonsubir" name="submit" type="submit" value="Mostrar Logs de Contenido"/>
                </form>
                <form enctype="multipart/form-data" name="testform" method='POST' action='Automatization.php'>
                <p class="text-justify">Ingresa el tema del contenido</p>

                <input id="nomTema" name="nomTema" type="text" required />

                <p class="text-justify">Este texto sera el principal (No mas de 550 Caracteres) Igual se bloquea cuando
                    llega a 550 asi que no hay problema</p>

                <textarea id="textoInicial" name="textoInicial" required /></textarea>

                <p class="text-justify">Ingresa el tipo de archivo multimedia que acompañara la pantalla de contenido
                </p>

                <input class="botonExa" required type="file" id="fileContent" name="fileContent[]" multiple
                    accept="image/*,video/*">

                <p class="text-justify">Por favor selecciona el tema al cual pertenece el contenido</p>


                <select name="tema" id="tema" onChange="mostrarAplicaciones();" required>

                    <option value="">Cuando selecciones una opcion... Mas opciones apareceran</option>

                    <option value="SocialMedia">Redes Sociales</option>

                    <option value="Movility">Movilidad</option>

                    <option value="GoogleTools">Herramientas de Google</option>

                    <option value="BasicHandle">Manejo Basico de Celulares</option>

                </select>

                <p>Por favor selecciona la aplicacion del contenido</p>


                <select name="SocialMedia" id="SocialMedia" style="display:none;">

                    <option value="">Selecciona una aplicacion</option>

                    <option value="Facebook">Facebook</option>

                    <option value="Whatsapp">Whatsapp</option>

                    <option value="Instagram">Instagram</option>

                    <option value="Twitter">Twitter</option>


                </select>
                <select name="Movility" id="Movility" style="display:none;">

                    <option value="">Selecciona una aplicacion</option>

                    <option value="Uber">Uber</option>

                    <option value="Beat">Beat</option>

                    <option value="Picap">Picap</option>

                    <option value="Cuper">Cuper</option>

                </select>
                <select name="GoogleTools" id="GoogleTools" style="display:none;">

                    <option value="">Selecciona una aplicacion</option>

                    <option value="Google_Music">Google Music</option>

                    <option value="Gmail">Gmail</option>

                    <option value="Google_Maps">Google Maps</option>

                    <option value="Drive">Drive</option>

                </select>
                <select name="BasicHandle" id="BasicHandle" style="display:none;">

                    <option value="">Selecciona una aplicacion</option>

                    <option value="Internet">Internet</option>

                    <option value="Contactos">Contactos</option>

                    <option value="Llamadas">Llamadas</option>

                    <option value="Mensajes">Mensajes</option>

                    <option value="Google_Play">Google Play</option>

                    <option value="Personalizacion">Personalización</option>

                    <option value="Galeria">Galeria</option>

                    <option value="Alarmas">Alarmas</option>

                    <option value="Camara">Camara</option>

                    <option value="Calculadora">Calculadora</option>

                </select>
                </br>
                <script>
                    function mostrarAplicaciones() {
                        var listaTema = document.getElementById("tema");
                        var opcion = listaTema.options[listaTema.selectedIndex];
                        switch (opcion.value) {
                            case "SocialMedia":
                                document.getElementById("SocialMedia").style.display = "block";
                                document.getElementById("Movility").style.display = "none";
                                document.getElementById("GoogleTools").style.display = "none";
                                document.getElementById("BasicHandle").style.display = "none";
                                document.getElementById("SocialMedia").required = true;
                                break;
                            case "Movility":
                                document.getElementById("SocialMedia").style.display = "none";
                                document.getElementById("Movility").style.display = "block";
                                document.getElementById("GoogleTools").style.display = "none";
                                document.getElementById("BasicHandle").style.display = "none";
                                document.getElementById("Movility").required = true;
                                break;
                            case "GoogleTools":
                                document.getElementById("SocialMedia").style.display = "none";
                                document.getElementById("Movility").style.display = "none";
                                document.getElementById("GoogleTools").style.display = "block";
                                document.getElementById("BasicHandle").style.display = "none";
                                document.getElementById("GoogleTools").required = true;
                                break;
                            case "BasicHandle":
                                document.getElementById("SocialMedia").style.display = "none";
                                document.getElementById("Movility").style.display = "none";
                                document.getElementById("GoogleTools").style.display = "none";
                                document.getElementById("BasicHandle").style.display = "block";
                                document.getElementById("BasicHandle").required = true;
                                break;
                        }
                    }
                </script>
                <input class="botonsubir" name="submit" type="submit" value="SUBIR INFORMACION" />
                </br>
                </br>
                <p class="text-justify">Esta parte de la pagina muestra una guia sobre como deberia quedar el contenido
                    nuevo
                    (no es
                    editable)</p>
        </div>
    </div>
    </form>
</body>
        <?php
    }
    else{
    include '../Persistencia/conexion.php';
    $contrasena = $_POST["contrasena"];
    $user = $_POST["usuario"];
    $contrasenapost = mysqli_real_escape_string($connection_mysql, $contrasena);
    $userpost = mysqli_real_escape_string($connection_mysql, $user);
    $sql = "SELECT * FROM PROFESORES where UsuarioProfesor='$userpost' AND ContrasenaProfesor='$contrasenapost'";
    $result = mysqli_query($connection_mysql,$sql);
    if (!empty($result) AND mysqli_num_rows($result) > 0) { ?>
<body>
    <div class="Todos">
        <div class="All">
            <div class="head">
                <h1>Nueva pantalla de contenidos</h1> <img id="Icono" src="../FrontEnd/Assets/icono.jpg" alt="Icono Migo">
            </div>
            <?php while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                        echo "<h1>Bienvenido Profesor: ".$row["NombreProfesor"]."</h1>";?>
                        <form enctype="multipart/form-data" name="testform" method='POST' action='BuscarProfesor.php'>
                            <?php echo '<input type="hidden" id="ProfesorID" name ="ProfesorID" value="'.$row["IDProfesor"].'"> ';?>
                            <input class="botonsubir" name="submit" type="submit" value="Editar mi informacion y clave"/>
                        </form>
                        <?php
                    }
                ?>
            <form enctype="multipart/form-data" name="testform" method='POST' action='Automatization.php'>
                <p class="text-justify">Ingresa el tema del contenido</p>
                <?php echo '<input type="hidden" id="IDProfesor" name ="IDProfesor" value="'.$row["IDProfesor"].'"> ';?>
                <input id="nomTema" name="nomTema" type="text" required />

                <p class="text-justify">Este texto sera el principal (No mas de 550 Caracteres) Igual se bloquea cuando
                    llega a 550 asi que no hay problema</p>

                <textarea id="textoInicial" name="textoInicial" required /></textarea>

                <p class="text-justify">Ingresa el tipo de archivo multimedia que acompañara la pantalla de contenido
                </p>

                <input class="botonExa" required type="file" id="fileContent" name="fileContent[]" multiple
                    accept="image/*,video/*">

                <p class="text-justify">Por favor selecciona el tema al cual pertenece el contenido</p>


                <select name="tema" id="tema" onChange="mostrarAplicaciones();" required>

                    <option value="">Cuando selecciones una opcion... Mas opciones apareceran</option>

                    <option value="SocialMedia">Redes Sociales</option>

                    <option value="Movility">Movilidad</option>

                    <option value="GoogleTools">Herramientas de Google</option>

                    <option value="BasicHandle">Manejo Basico de Celulares</option>

                </select>

                <p>Por favor selecciona la aplicacion del contenido</p>


                <select name="SocialMedia" id="SocialMedia" style="display:none;">

                    <option value="">Selecciona una aplicacion</option>

                    <option value="Facebook">Facebook</option>

                    <option value="Whatsapp">Whatsapp</option>

                    <option value="Instagram">Instagram</option>

                    <option value="Twitter">Twitter</option>


                </select>
                <select name="Movility" id="Movility" style="display:none;">

                    <option value="">Selecciona una aplicacion</option>

                    <option value="Uber">Uber</option>

                    <option value="Beat">Beat</option>

                    <option value="Picap">Picap</option>

                    <option value="Cuper">Cuper</option>

                </select>
                <select name="GoogleTools" id="GoogleTools" style="display:none;">

                    <option value="">Selecciona una aplicacion</option>

                    <option value="Google_Music">Google Music</option>

                    <option value="Gmail">Gmail</option>

                    <option value="Google_Maps">Google Maps</option>

                    <option value="Drive">Drive</option>

                </select>
                <select name="BasicHandle" id="BasicHandle" style="display:none;">

                    <option value="">Selecciona una aplicacion</option>

                    <option value="Internet">Internet</option>

                    <option value="Contactos">Contactos</option>

                    <option value="Llamadas">Llamadas</option>

                    <option value="Mensajes">Mensajes</option>

                    <option value="Google_Play">Google Play</option>

                    <option value="Personalizacion">Personalización</option>

                    <option value="Galeria">Galeria</option>

                    <option value="Alarmas">Alarmas</option>

                    <option value="Camara">Camara</option>

                    <option value="Calculadora">Calculadora</option>

                </select>
                </br>
                <script>
                    function mostrarAplicaciones() {
                        var listaTema = document.getElementById("tema");
                        var opcion = listaTema.options[listaTema.selectedIndex];
                        switch (opcion.value) {
                            case "SocialMedia":
                                document.getElementById("SocialMedia").style.display = "block";
                                document.getElementById("Movility").style.display = "none";
                                document.getElementById("GoogleTools").style.display = "none";
                                document.getElementById("BasicHandle").style.display = "none";
                                document.getElementById("SocialMedia").required = true;
                                break;
                            case "Movility":
                                document.getElementById("SocialMedia").style.display = "none";
                                document.getElementById("Movility").style.display = "block";
                                document.getElementById("GoogleTools").style.display = "none";
                                document.getElementById("BasicHandle").style.display = "none";
                                document.getElementById("Movility").required = true;
                                break;
                            case "GoogleTools":
                                document.getElementById("SocialMedia").style.display = "none";
                                document.getElementById("Movility").style.display = "none";
                                document.getElementById("GoogleTools").style.display = "block";
                                document.getElementById("BasicHandle").style.display = "none";
                                document.getElementById("GoogleTools").required = true;
                                break;
                            case "BasicHandle":
                                document.getElementById("SocialMedia").style.display = "none";
                                document.getElementById("Movility").style.display = "none";
                                document.getElementById("GoogleTools").style.display = "none";
                                document.getElementById("BasicHandle").style.display = "block";
                                document.getElementById("BasicHandle").required = true;
                                break;
                        }
                    }
                </script>
                <input class="botonsubir" name="submit" type="submit" value="SUBIR INFORMACION" />
                </br>
                </br>
                <p class="text-justify">Esta parte de la pagina muestra una guia sobre como deberia quedar el contenido
                    nuevo
                    (no es
                    editable)</p>
        </div>
    </div>
    </form>
</body>

<?php 
    }else{
        echo '<body><div class="Todos"><div class="All"><p>fallo la autenticacion, por favor vuelve</p></div></div></body>';
        }
    }
}else{
    echo '<body><div class="Todos"><div class="All"><p>No ingresaste un caracter valido en el usuario o la contraseña</p></div></div></body>';
}?>
</html>