<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" id="ArchivoCss" type="text/css" href="../FrontEnd/Assets/stylo.css">
    <title>Pagina de Contenidos</title>
</head>
    <?php
    include '../Persistencia/conexion.php';
    $sql = 'SELECT * FROM LOGS_CONTENIDO';
    echo $sql;
    $result = mysqli_query($connection_mysql,$sql);
    if (!empty($result) AND mysqli_num_rows($result) > 0) { ?>
<body>
    <div class="Todos">
        <div class="All">
            <div class="head">
                <h1>Resultado de Contenidos</h1> <img id="Icono" src="../FrontEnd/Assets/icono.jpg" alt="Icono Migo">
            </div>
            <table class="egt">
                        <tr>
                            <th>Nombre Contenido</th>
                            <th>Tema Contenido</th>
                            <th>Aplicacion Contenido</th>
                            <th>ID Profesor Encargado</th>
                            <th>Fecha Contenido</th>
                        </tr>
                        <?php while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) { ?>
                            <tr>
                                <td><?php echo $row["NombreContenido"] ?></td>
                                <td><?php echo $row["TemaContenido"] ?></td>
                                <td><?php echo $row["AplicacionContenido"] ?></td>
                                <td><?php echo $row["IDProfesor"] ?></td>
                                <td><?php echo $row["FechaContenido"] ?></td>
                            </tr>
                        <?php }
                    }
                    ?>
            </table>
        </div>
    </div>
</body>
</html>