<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" id="ArchivoCss" type="text/css" href="../FrontEnd/Assets/stylo.css">
    <title>Pagina de Busqueda de Profesores</title>
</head>
    <?php
    include '../Persistencia/conexion.php';
    if(isset($_POST["ProfesorID"]) && $_POST["ProfesorID"] != ''){
        $profesorID = $_POST["ProfesorID"];
        $profesorIDpost = mysqli_real_escape_string($connection_mysql, $profesorID);
        $sql = 'SELECT * FROM PROFESORES where IDProfesor = '.$profesorIDpost;
    }elseif(isset($_POST["ProfesorNombre"]) && $_POST["ProfesorNombre"] != ''){
        $profesorNombre = $_POST["ProfesorNombre"];
        $profesorNombrepost = mysqli_real_escape_string($connection_mysql, $profesorNombre);
        $sql = 'SELECT * FROM PROFESORES where NombreProfesor like ("%'.$profesorNombre.'%")';
    }
    $result = mysqli_query($connection_mysql,$sql);
    if (!empty($result) AND mysqli_num_rows($result) > 0) { ?>
<body>
    <div class="Todos">
        <div class="All">
            <div class="head">
                <h1>Resultado de Busqueda de profesores</h1> <img id="Icono" src="../FrontEnd/Assets/icono.jpg" alt="Icono Migo">
            </div>
                <?php
                    while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) { ?>
                    <form enctype="multipart/form-data" name="testform" method='POST' action='../BackEnd/EditarInformacion.php'>
                        <p class="text-justify">Ingresa tu nombre: </p>
                        <input id="nombre" value="<?php echo $row["NombreProfesor"] ?>" name="nombre" type="text" required />
                        <p class="text-justify">Ingresa tu Apellido: </p>
                        <input id="apellido" value="<?php echo $row["ApellidoProfesor"] ?>" name="apellido" type="text" required />
                        <p class="text-justify">Ingresa tu Profesion: </p>
                        <input id="profesion" value="<?php echo $row["ProfesionProfesor"] ?>" name="profesion" type="text" required />
                        <p class="text-justify">Ingresa tu Edad: </p>
                        <input id="edad" value="<?php echo $row["EdadProfesor"] ?>" name="edad" type="text" required />
                        <p class="text-justify">Ingresa tu Usuario: </p>
                        <input id="usuario" value="<?php echo $row["UsuarioProfesor"] ?>" name="usuario" type="text" required />
                        <p class="text-justify">Ingresa tu Contraseña: </p>
                        <input id="contrasena" value="<?php echo $row["ContrasenaProfesor"] ?>" name="contrasena" type="text" required />
                        <?php echo '<input type="hidden" id="IDProfesor" name ="IDProfesor" value="'.$row["IDProfesor"].'"> ';?>
                        <input class="botonsubir" name="submit" type="submit" value="SUBIR INFORMACION" />
                    </form>
                    <?php }
            }
            ?>
        </div>
    </div>
</body>
</html>